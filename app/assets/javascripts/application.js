// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs 

//= require activestorage

//= require_tree .


var Craig = function(){
	var ctu = this;

	this.checkStringisUrl = function(element){
		var eleComponent = $(element);
		var str = eleComponent.val();
		  var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
		  if(!regex.test(str)) {
		    eleComponent.parent().parent().find('input[type="submit"]').attr('disabled', 'disabled');
		    return false;
		  } else {
		  	console.log("heee")
		  	eleComponent.parent().parent().find('input[type="submit"]').removeAttr('disabled');
		    return true;
		  }
	};
	

	this.onkeyupcheck = function(ele){
		$(document).on("keyup" , ele, function(){
			ctu.checkStringisUrl(ele);
		})
		$(document).on("change" , ele, function(){
			ctu.checkStringisUrl(ele);
		})
	};


	this.copyToClipboard = function(cls){
	};


};


$(document).ready(function(){
	var craigObj = new Craig;
	craigObj.onkeyupcheck("#url_long_url")
	craigObj.copyToClipboard("copy-to-clip")
})




