class Dashboard::UrlsController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_url, only: [:edit, :update, :destroy, :show]

	def index # to get a new object of url and already existed urls for current user
		@urls = current_user.urls
		@url = Url.new
	end

	def create # to 
		@url = current_user.urls.new(url_params)

		if @url.save
			flash[:success] = "Url has been successfully shortened"
			redirect_to dashboard_urls_path
		else
			flash[:error] = "#{@url.errors.full_messages.join(',')}"
			redirect_back(fallback_location: root_url)
		end
	end

	def edit		
	end

	def update # action to update the title of url
		if @url.update(url_params)
			flash[:success] = "Url has been updated successfully"
			redirect_back(fallback_location: root_url)
		else
			flash[:error] = "#{@url.errors.full_messages.join(',')}"
			redirect_back(fallback_location: root_url)
		end
	end

	def show
		
	end

	def destroy
		if @url.destroy
			flash[:success] = "Url has been deleted successfully"
			redirect_to dashboard_urls_path
		else
			flash[:error] = "#{@url.errors.full_messages.join(',')}"
			redirect_to dashboard_urls_path
		end 	
	end

	private

		def url_params
			params.require(:url).permit(:long_url, :title)
		end

		def set_url # common method to set url object
			@url = current_user.urls.find_by(token: params[:id])
		end
end
