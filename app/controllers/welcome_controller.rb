class WelcomeController < ApplicationController
	before_action :check_user_is_guest?, except: :show
	include ApplicationHelper

	def index
		@url = Url.new
	end

	# action to redirect shorted link to actual link
	def show 
		@url = Url.find_by(token: params[:id])
		if @url.present?
			redirect_to @url.long_url
		end
	end

	# action to check existing link for this long_url and create if does not exist
	def create 
		@existing_url = Url.find_by(long_url: params[:url][:long_url])			
		
		if @existing_url.present?	
			@existing_url.session_urls.find_or_create_by(session_id: session.id)
			flash[:warning] = "Url has been shortened"
			redirect_back(fallback_location: root_url)
		else
			@url = Url.new(url_params)	
			if @url.save
				flash[:success] = "Url has shortened successfully"
				redirect_back(fallback_location: root_url)
			else
				flash[:error] = "#{@url.errors.full_messages.join(',')}"
				render (:index)
			end
		end
	end	

	private
		
		def url_params
			params.require(:url).permit(:long_url, :session_id)
		end

		def check_user_is_guest? # before action to check if user is logged in then switch to dashboard url controller
			if user_signed_in? 
				redirect_to dashboard_urls_path
			end
		end
end
