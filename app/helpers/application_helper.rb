module ApplicationHelper
	def urls_from_session
		session_urls = SessionUrl.where(session_id: session.id).order("created_at desc")
		session_urls.collect(&:url).compact
	end
end
