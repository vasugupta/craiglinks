class SessionUrl < ApplicationRecord
	# this class used to establish the relation between session and url has_many  
	belongs_to :url
end
