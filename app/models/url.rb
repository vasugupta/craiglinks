class Url < ApplicationRecord
	# this class is using to store all records for Url data 
	validates_presence_of :long_url
	validates_uniqueness_of :long_url, scope: :user_id, if: Proc.new{self.user_id.present?}	# condition for instance if having user then check uniqueness of long_url
	
	belongs_to :user, optional: true
	has_many :session_urls # assocition needed in case of session user only to ignore creation of multiple url for the same long_url 

	before_create :create_token # callback to set a unique token for minified url
	before_create :create_session_url # create association  


	def to_param
		token
	end

	# use to set the value of token secure 
	def create_token
		self.token = SecureRandom.hex(4)
	end

	# to make the associated object of session_urls table if user is not logged in
	def create_session_url
		if self.session_id.present?
			self.session_urls.new(session_id: self.session_id)
		end
	end

end
