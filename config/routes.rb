Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "welcome#index"
  # post "/", to: "welcome#crae"
  resources :welcome, path: ""

  authenticated :user do
    root 'dashboard/urls#index'
  end

  namespace :dashboard do 
  	resources :urls
  end
end
