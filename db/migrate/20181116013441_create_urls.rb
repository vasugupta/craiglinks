class CreateUrls < ActiveRecord::Migration[5.2]
  def change
    create_table :urls do |t|
      t.string :title
      t.integer :user_id
      t.string :session_id
      t.string :long_url
      t.string :token, null: false, unique: true

      t.timestamps
    end
  end
end
