class CreateSessionUrls < ActiveRecord::Migration[5.2]
  def change
    create_table :session_urls do |t|
      t.string :session_id
      t.integer :url_id

      t.timestamps
    end
  end
end
